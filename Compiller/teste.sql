#teste de select simples asterisco
select * from tbl_alunos;

--teste select simples com colunas
select id, nome from tbl_alunos;

--teste select com where simples
select id, nome from tbl_alunos where turma = '3E';

--teste select com where like
select id, nome from tbl_alunos where nome LIKE '%Ana%';



/* teste de create  com if not existes */
CREATE TABLE IF NOT EXISTS AtendimentoSquadXCliente32 (
    id char NOT NULL,
    squad double precision,
    isUsed numeric NOT NULL DEFAULT 1,
     PRIMARY KEY (id)
    );


/* teste de insert correto com um valor */
INSERT INTO AtendimentoSquadXCliente32 (id, usuario, isUsed)  VALUES (3, 2, 1);
INSERT INTO AtendimentoSquadXCliente32 (id, usuario, isUsed)  VALUES (3, 2, 1);

