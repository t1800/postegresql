package compiller;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import scanner.Scanner;
import parser.Parser;
import erros.ListaErros;
/**
 *
 * @author Willian Castro
 */
public class Compiller {
    public static String addEspaco(int quantidade){
        String retorno = "";
        for (int i = 0; i < quantidade; i++) {
            retorno += " ";
        }
        return retorno;
    }

    public static String stringArquivo(String arquivo){
        String retorno = "";
        String nomeArquivo[] = arquivo.split("/");
        arquivo = nomeArquivo[(nomeArquivo.length)-1];
        if(arquivo.length() < 17){
            int qtd = (17 - arquivo.length())/2 ;
            int resto = ((17-arquivo.length())%2);
            retorno += addEspaco(qtd) + arquivo + addEspaco(qtd);
            retorno += addEspaco(resto);
        }
        return "|"+retorno;
    }

    public static String stringResultado(boolean resultado){
        if(resultado){
            return "|   Correto   |";
        }
        return "|  Incorreto  |";
    }

    public static String stringTempo(float tempo){
        String retorno = "";
        String temp = Float.toString(tempo);
        int qtd = (28-temp.length())/2;
        int resto = ((28-temp.length())%2)+1 ;
        retorno += addEspaco(qtd) + temp + addEspaco(qtd);
        return retorno + addEspaco(resto) +"|";
    }

    public static String montaString(String arquivo, boolean resultado, float tempo, String erros) {
        String retorno = "";
        retorno += stringArquivo(arquivo);
        if(erros == ""){
            erros = addEspaco(16)+"Nenhum";
        }
        retorno += stringResultado(resultado);
        retorno += stringTempo(tempo);
        return retorno +" "+ erros;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        List<String> arquivos = Arrays.asList(
                "test/selects/s1.txt", "test/selects/s2.txt", "test/selects/s3.txt", "test/selects/s4.txt",
                "test/selects/s5.txt", "test/selects/s6.txt", "test/selects/s7.txt", "test/selects/s8.txt",
                "test/selects/s9.txt", "test/selects/s10.txt", "test/selects/s11.txt", "test/selects/s12.txt",
                "test/selects/s13.txt", "test/selects/s14.txt",

                "test/alters/a1.txt", "test/alters/a2.txt","test/alters/a3.txt", "test/alters/a4.txt",
                "test/alters/a5.txt", "test/alters/a6.txt","test/alters/a7.txt","test/alters/a8.txt",

                "test/creates/c1.txt", "test/creates/c2.txt", "test/creates/c3.txt", "test/creates/c4.txt", "test/creates/c5.txt",

                "test/updates/u1.txt", "test/updates/u2.txt",

                "test/inserts/i1.txt", "test/inserts/i2.txt","test/inserts/i3.txt",

                "test/deletes/d1.txt", "test/deletes/d2.txt",

                "test/createDB.txt", "test/truncate.txt", "test/dropTable.txt", "test/dropDB.txt"
        );
        List<String> saidas = new ArrayList<>();

        for (int i = 0; i < arquivos.size() ; i++) {
            ListaErros listaErros = new ListaErros();
            FileReader in = new FileReader(arquivos.get(i));

            try {
                Scanner scanner = new Scanner(in, listaErros);
                Parser parser = new Parser(scanner);

                long startTime = System.nanoTime();
                parser.parse();

                long endTime = System.nanoTime();
                long timeElapsed = endTime - startTime;
                timeElapsed = timeElapsed/1000;

                if (!listaErros.hasErros()) {
                    saidas.add(montaString(arquivos.get(i), true, timeElapsed, ""));

                } else {
                    saidas.add(montaString(arquivos.get(i),false, timeElapsed, listaErros.dumpString()));
                }
            }catch (Exception e){
                saidas.add(montaString(arquivos.get(i),false, 0, "Erro de Parser"));
            }
        }
        FileWriter arq = new FileWriter("saida.txt");
        PrintWriter gravarArq = new PrintWriter(arq);
        String str = "";
        str +=       "|                          Tabela de resultados PostgreSQL                                               |\n";
        str +=       "|--------------------------------------------------------------------------------------------------------\n";
        str +=       "|     Arquivo     |  Resultado  |         Tempo(microS)       |                 Erros \n";
        str +=       "|-----------------+-------------+-----------------------------+------------------------------------------\n";
        for (int i = 0; i < saidas.size(); i++) {
            str += saidas.get(i)+"\n";
        }
        str += "|-----------------+-------------+-----------------------------+------------------------------------------";
        gravarArq.print(str);
        arq.close();
    }
}
