package erros;
import erros.Erro;
import java.util.ArrayList;

public class ListaErros {
    ArrayList<Erro> erros;

    public ListaErros() {
        this.erros = new ArrayList<Erro>();
    }

    //Usado quando sabemos de fato a linha e coluna exata
    public void defineErro(int linha, int coluna, String texto) {
        Erro e = new Erro(linha, coluna, texto);
        this.erros.add(e);
    }

    /*  Sobrecarga do método defineErro()
     * - Este método foi criado por causa do syntax_error()
     * - Ou seja, será usado pelo syntax_error()
     */
    public void defineErro(int linha, int coluna) {
        Erro e = new Erro(linha, coluna);
        this.erros.add(e);
    }

    /*
     * Adiciona o texto no primeiro erro sem descricao que encontrar.
     * - Este método foi criado por causa do syntax_error()
     * - Ou seja, naqueles erros que o syntax_error() preencheu o local. O
     * syntax_error() será apresentado mais adiante
     */
    public void defineErro(String texto) {
        for (Erro e: this.erros){
            if (e.getTexto() == null) {
                e.setTexto(texto);
                return;
            }
        }
    }

    /**
     *      Imprime erro por erro.
     * Não chamamos imprimir por uma questão de semantica apenas
     * Como estamos falando de erros, uma tradução mais interessante é dump que é 'despejo'
     * ou seja, na computação é um termo muito usado para relatorio de erros.
     */
    public void dump() {
        for (Erro e: this.erros) {
            e.imprime();
        }
    }

    public String dumpString() {
        String str = "";
        for (Erro e: this.erros) {
            str = e.toString();
        }
        return str;
    }

    public boolean hasErros(){
        if(this.erros.size() > 0){
            return true;
        }
        return false;
    }
}
