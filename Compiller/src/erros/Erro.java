package erros;

public class Erro {
    int linha, coluna;
    String texto;

    //Sobrecarga do construtor
    Erro(){
        this.linha = -1;
        this.coluna = -1;
        this.texto = "";
    }

    //Sobrecarga do construtor
    Erro(int linha, int coluna, String texto) {
        this.linha = linha;
        this.coluna = coluna;
        this.texto = texto;
    }

    //Sobrecarga do construtor
    Erro(int linha, int coluna) {
        this.linha = linha;
        this.coluna = coluna;
        this.texto = null;//"Erro não definido";
    }

    public int getLinha() {
        return linha;
    }

    public void setLinha(int linha) {
        this.linha = linha;
    }

    public int getColuna() {
        return coluna;
    }

    public void setColuna(int coluna) {
        this.coluna = coluna;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public void imprime(){
        String aux = "linha:" + this.linha +", coluna:" + this.coluna + ", ";
        if(this.texto == null) {
            aux += "erro indefinido!";
        }else {
            aux += this.texto;
        }
        System.out.println(aux);
    }

    public String toString(){
        String aux = "";
        if(this.texto == null) {
            aux += "erro indefinido!";
        }else {
            aux += this.texto;
        }
        return aux;
    }
}

