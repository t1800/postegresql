package scanner;
import java_cup.runtime.*;
import parser.sym;
import erros.ListaErros;



%%
%cup
%class Scanner
%public
%eofval{
    return criaSimbolo(sym.EOF);
%eofval}

%{
    //atributo
    private ListaErros listaErros;

    //Redefinindo "sobrecarga" o construtor de Scanner inicializar a listaErros
    public Scanner(java.io.FileReader in, ListaErros listaErros) {
        this(in);
        this.listaErros = listaErros;
    }

    public ListaErros getListaErros() {
        return listaErros;
    }

    public void defineErro(int linha, int coluna, String texto) {
        listaErros.defineErro(linha, coluna, texto);
    }

    public void defineErro(int linha, int coluna) {
        listaErros.defineErro(linha, coluna);
    }

    public void defineErro(String texto) {
        listaErros.defineErro(texto);
    }
    private Symbol criaSimbolo(int code, Object value) {
        return new Symbol(code, yyline, yycolumn, value);
    }
    private Symbol criaSimbolo(int code) {
        return new Symbol(code, yyline, yycolumn, null);
    }

%}

Letra = [A-Za-z]
Digito = [0-9]
fimdeLinha = \r|\n|\r\n
espaco = {fimdeLinha} | [\t|\f|" "]+
Inteiro = [0|[1-9][0-9]]*
Comparators = [>|<|>=|<=|<>]
Between = [b|B][e|E][t|T][w|W][e|E][e|E][n|N]

Create = [c|C][r|R][e|E][a|A][t|T][e|E]
Insert = [i|I][n|N][s|S][e|E][r|R][t|T]
Into = [i|I][n|N][t|T][o|O]
Values = [v|V][a|A][l|L][u|U][e|E][s|S]
Alter = [a|A][l|L][t|T][e|E][r|R]
Add = [a|A][d|D][d|D]
Table = [t|T][a|A][b|B][l|L][e|E]
Column = [c|C][o|O][l|L][u|U][m|M][n|N]
If = [i|I][f|F]
Exists = [e|E][x|X][i|I][s|S][t|T][s|S]
Select = [s|S][e|E][l|L][e|E][c|C][t|T]
From = [f|F][r|R][o|O][m|M]
Not = [n|N][o|O][t|T]
Null = [n|N][u|U][l|L][l|L]
Default = [d|D][e|E][f|F][a|A][u|U][l|L][t|T]
Primary = [p|P][r|R][i|I][m|M][a|A][r|R][y|Y]
Foreign = [f|F][o|O][r|R][e|E][i|I][g|G][n|N]
References = [r|R][e|E][f|F][e|E][r|R][e|E][n|N][c|C][e|E][s|S]
Constraint = [c|C][o|O][n|N][s|S][t|T][r|R][a|A][i|I][n|N][t|T]
Key = [k|K][e|E][y|Y]
Type = [t|T][y|Y][p|P][e|E]
Unique = [u|U][n|N][i|I][q|Q][u|U][e|E]
Update = [u|U][p|P][d|D][a|A][t|T][e|E]
Set = [s|S][e|E][t|T]
Delete = [d|D][e|E][l|L][e|E][t|T][e|E]
Distinct = [d|D][i|I][s|S][t|T][i|I][n|N][c|C][t|T]
Drop = [d|D][r|R][o|O][p|P]
Truncate = [t|T][r|R][u|U][n|N][c|C][a|A][t|T][e|E]
To = [t|T][o|O]
Database = [d|D][a|A][t|T][a|A][b|B][a|A][s|S][e|E]
Count = [c|C][o|O][u|U][n|N][t|T]
Min = [m|M][i|I][n|N]
Max = [m|M][a|A][x|X]
Avg = [a|A][v|V][g|G]
Sum = [s|S][u|U][m|M]
And = [a|A][n|N][d|D]
Or = [o|O][r|R]
Like = [l|L][i|I][k|K][e|E]
In = [i|I][n|N]
As = [a|A][s|S]
Is = [i|I][s|S]
CurrentDate = [c|C][u|U][r|R][r|R][e|E][n|N][t|T][_][d|D][a|A][t|T][e|E]
Where = [w|W][h|H][e|E][r|R][e|E]
Order = [o|O][r|R][d|D][e|E][r|R]
By = [b|B][y|Y]
Asc = [a|A][s|S][c|C]
Desc = [d|D][e|E][s|S][c|C]
Limit = [l|L][i|I][m|M][i|I][t|T]


TiposInteiros = [i|I][n|N][t|T]
    |[b|B][i|I][g|G][i|I][n|N][t|T]
    |[i|I][n|N][t|T][e|E][g|G][e|E][r|R]
    |[m|M][e|E][d|D][i|I][u|U][m|M][i|I][n|N][t|T]
    |[m|M][i|I][d|D][d|D][l|L][e|E][i|I][n|N][t|T]
    |[s|S][m|M][a|A][l|L][l|L][i|I][n|N][t|T]

TiposDecimais = [r|R][e|E][a|A][l|L]
    |[d|D][o|O][u|U][b|B][l|L][e|E]{espaco}[p|P][r|R][e|E][c|C][i|I][s|S][i|I][o|O][n|N]
    |[d|D][e|E][c|C][i|I][m|M][a|A][l|L]
    |[n|N][u|U][m|M][e|E][r|R][i|I][c|C]
    |[f|F][l|L][o|O][a|A][t|T]
    |[s|S][m|M][a|A][l|L][l|L][s|S][e|E][r|R][i|I][a|A][l|L]
    |[s|S][e|E][r|R][i|I][a|A][l|L]
    |[b|B][i|I][g|G][s|S][e|E][r|R][i|I][a|A][l|L]

TiposBooleanos = [b|B][i|I][t|T]
    |[b|B][i|I][t|T]{espaco}[v|V][a|A][r|R][y|Y][i|I][n|N][g|G]
    |[b|B][o|O][o|O][l|L][e|E][a|A][n|N]


TiposNumericos = {TiposDecimais}
    |{TiposBooleanos}


TiposTempo = [d|D][a|A][t|T][e|E]
    |[t|T][i|I][m|M][e|E]
    |[t|T][i|I][m|M][e|E][s|S][t|T][a|A][m|M][p|P]


TiposCharacters = [c|C][h|H][a|A][r|R]
    |[c|C][h|H][a|A][r|R][a|A][c|C][t|T][e|E][r|R]
    |[c|C][h|H][a|A][r|R][a|A][c|C][t|T][e|E][r|R]{espaco}[v|V][a|A][r|R][y|Y][i|I][n|N][g|G]
    |[v|V][a|A][r|R][c|C][h|H][a|A][r|R]
    |[t|T][e|E][x|X][t|T]
    |[t|T][i|I][n|N][y|Y][t|T][e|E][x|X][t|T]

Tipos = {TiposNumericos}
    |{TiposTempo}
    |{TiposCharacters}


Rename = [r|R][e|E][n|N][a|A][m|M][e|E]

Reservadas = [a|A][l|L][l|L]
             |[a|A][n|N][a|A][l|L][y|Y][s|S][e|E]
             |[a|A][n|N][y|Y]
             |[a|A][r|R][r|R][a|A][y|Y]
             |[a|A][s|S][y|Y][m|M][m|M][e|E][t|T][r|R][i|I][c|C]
             |[a|A][u|U][t|T][h|H][o|O][r|R][i|I][z|Z][a|A][t|T][i|I][o|O][n|N]
             |[b|B][i|I][n|N][a|A][r|R][y|Y]
             |[b|B][o|O][t|T][h|H]
             |[c|C][a|A][s|S][e|E]
             |[c|C][a|A][s|S][t|T]
             |[c|C][h|H][e|E][c|C][k|K]
             |[c|C][o|O][l|L][l|L][a|A][t|T][e|E]
             |[c|C][o|O][l|L][l|L][a|A][t|T][i|I][o|O][n|N]
             |[c|C][o|O][n|N][c|C][u|U][r|R][r|R][e|E][n|N][t|T][l|L][y|Y]
             |[c|C][r|R][o|O][s|S][s|S]
             |[c|C][u|U][r|R][r|R][e|E][n|N][t|T][_][c|C][a|A][t|T][a|A][l|L][o|O][g|G]
             |[c|C][u|U][r|R][r|R][e|E][n|N][t|T][_][r|R][o|O][l|L][e|E]
             |[c|C][u|U][r|R][r|R][e|E][n|N][t|T][_][s|S][c|C][h|H][e|E][m|M][a|A]
             |[c|C][u|U][r|R][r|R][e|E][n|N][t|T][_][t|T][i|I][m|M][e|E]
             |[c|C][u|U][r|R][r|R][e|E][n|N][t|T][_][t|T][i|I][m|M][e|E][s|S][t|T][a|A][m|M][p|P]
             |[c|C][u|U][r|R][r|R][e|E][n|N][t|T][_][u|U][s|S][e|E][r|R]
             |[d|D][e|E][f|F][e|E][r|R][r|R][a|A][b|B][l|L][e|E]
             |[d|D][o|O]
             |[e|E][l|L][s|S][e|E]
             |[e|E][x|X][c|C][e|E][p|P][t|T]
             |[f|F][a|A][l|L][s|S][e|E]
             |[f|F][e|E][t|T][c|C][h|H]
             |[f|F][o|O][r|R]
             |[f|F][r|R][e|E][e|E][z|Z][e|E]
             |[f|F][u|U][l|L][l|L]
             |[g|G][r|R][a|A][n|N][t|T]
             |[g|G][r|R][o|O][u|U][p|P]
             |[h|H][a|A][v|V][i|I][n|N][g|G]
             |[i|I][n|N][n|N][e|E][r|R]
             |[i|I][n|N][t|T][e|E][r|R][s|S][e|E][c|C][t|T]
             |[i|I][s|S][n|N][u|U][l|L][l|L]
             |[j|J][o|O][i|I][n|N]
             |[l|L][a|A][t|T][e|E][r|R][a|A][l|L]
             |[l|L][e|E][a|A][d|D][i|I][n|N][g|G]
             |[l|L][e|E][f|F][t|T]
             |[l|L][o|O][c|C][a|A][l|L][t|T][i|I][m|M][e|E]
             |[l|L][o|O][c|C][a|A][l|L][t|T][i|I][m|M][e|E][s|S][t|T][a|A][m|M][p|P]
             |[n|N][a|A][t|T][u|U][r|R][a|A][l|L]
             |[n|N][o|O][t|T][n|N][u|U][l|L][l|L]
             |[o|O][n|N]
             |[o|O][n|N][l|L][y|Y]
             |[o|O][u|U][t|T][e|E][r|R]
             |[o|O][v|V][e|E][r|R][l|L][a|A][p|P][s|S]
             |[p|P][l|L][a|A][c|C][i|I][n|N][g|G]
             |[r|R][e|E][t|T][u|U][r|R][n|N][i|I][n|N][g|G]
             |[r|R][i|I][g|G][h|H][t|T]
             |[s|S][e|E][s|S][s|S][i|I][o|O][n|N][_][u|U][s|S][e|E][r|R]
             |[s|S][i|I][m|M][i|I][l|L][a|A][r|R]
             |[s|S][o|O][m|M][e|E]
             |[s|S][y|Y][m|M][m|M][e|E][t|T][r|R][i|I][c|C]
             |[t|T][a|A][b|B][l|L][e|E][s|S][a|A][m|M][p|P][l|L][e|E]
             |[t|T][h|H][e|E][n|N]
             |[t|T][r|R][a|A][i|I][l|L][i|I][n|N][g|G]
             |[t|T][r|R][u|U][e|E]
             |[u|U][n|N][i|I][o|O][n|N]
             |[u|U][s|S][e|E][r|R]
             |[u|U][s|S][i|I][n|N][g|G]
             |[v|V][a|A][r|R][i|I][a|A][d|D][i|I][c|C]
             |[v|V][e|E][r|R][b|B][o|O][s|S][e|E]
             |[w|W][h|H][e|E][n|N]
             |[w|W][i|I][n|N][d|D][o|O][w|W]
             |[w|W][i|I][t|T][h|H]


Ident = {Letra}({Letra}|{Digito}| "_")*

String = (\"(.*)\")|('(.*)')

Comentario = (--(.*)[\n]?)
    |(#(.*)[\n]?)
    |(\/\*([^]*)\*\/)


%%

{Reservadas} {
          String aux = yytext();
          return criaSimbolo(sym.RESERVED, new String(aux));
}

{Comentario} {/* despresa */}

{Inteiro} {
    float aux = Float.parseFloat(yytext());
    return criaSimbolo(sym.INTEIRO, new Float(aux));
}

"=" { return criaSimbolo(sym.IGUAL);}

{Comparators} {
    String aux = yytext();
    return criaSimbolo(sym.COMPARATORS, new String(aux));
}

{String} {
    String aux = yytext();
    return criaSimbolo(sym.STRING, new String(aux));
}

{Between} {
    String aux = yytext();
    return criaSimbolo(sym.BETWEEN, new String(aux));
}

"*" { return criaSimbolo(sym.MULT);}

"(" { return criaSimbolo(sym.ABREP);}

")" { return criaSimbolo(sym.FECHAP);}

"," { return criaSimbolo(sym.VIRG);}

";" { return criaSimbolo(sym.PTVIRG);}


/*  Parte dedicada ao sql */
{Select} {
    String aux = yytext();
    return criaSimbolo(sym.SELECT, new String(aux));
}

{From} {
    String aux = yytext();
    return criaSimbolo(sym.FROM, new String(aux));
}

{Create} {
    String aux = yytext();
    return criaSimbolo(sym.CREATE, new String(aux));
}

{Insert} {
    String aux = yytext();
    return criaSimbolo(sym.INSERT, new String(aux));
}

{Into} {
    String aux = yytext();
    return criaSimbolo(sym.INTO, new String(aux));
}

{Values} {
    String aux = yytext();
    return criaSimbolo(sym.VALUES, new String(aux));
}

{Table} {
    String aux = yytext();
    return criaSimbolo(sym.TABLE, new String(aux));
}

{If} {
    String aux = yytext();
    return criaSimbolo(sym.IF, new String(aux));
}

{Column} {
    String aux = yytext();
    return criaSimbolo(sym.COLUMN, new String(aux));
}

{Exists} {
    String aux = yytext();
    return criaSimbolo(sym.EXISTS, new String(aux));
}


{Tipos} {
    String aux = yytext();
    return criaSimbolo(sym.TIPOS, new String(aux));
}

{TiposInteiros} {
    String aux = yytext();
    return criaSimbolo(sym.TIPOS_INT, new String(aux));
}

{Primary} {
    String aux = yytext();
    return criaSimbolo(sym.PRIMARY, new String(aux));
}

{Foreign} {
    String aux = yytext();
    return criaSimbolo(sym.FOREIGN, new String(aux));
}

{References} {
    String aux = yytext();
    return criaSimbolo(sym.REFERENCES, new String(aux));
}

{Key} {
    String aux = yytext();
    return criaSimbolo(sym.KEY, new String(aux));
}

{Type} {
    String aux = yytext();
    return criaSimbolo(sym.TYPE, new String(aux));
}

{Constraint} {
    String aux = yytext();
    return criaSimbolo(sym.CONSTRAINT, new String(aux));
}

{Unique} {
    String aux = yytext();
    return criaSimbolo(sym.UNIQUE, new String(aux));
}

{Alter} {
    String aux = yytext();
    return criaSimbolo(sym.ALTER, new String(aux));
}

{Add} {
    String aux = yytext();
    return criaSimbolo(sym.ADD, new String(aux));
}

{Where} {
    String aux = yytext();
    return criaSimbolo(sym.WHERE, new String(aux));
}

{Order} {
    String aux = yytext();
    return criaSimbolo(sym.ORDER, new String(aux));
}

{Rename} {
    String aux = yytext();
    return criaSimbolo(sym.RENAME, new String(aux));
}

{By} {
    String aux = yytext();
    return criaSimbolo(sym.BY, new String(aux));
}

{Asc} {
    String aux = yytext();
    return criaSimbolo(sym.ASC, new String(aux));
}

{Desc} {
    String aux = yytext();
    return criaSimbolo(sym.DESC, new String(aux));
}

{Limit} {
    String aux = yytext();
    return criaSimbolo(sym.LIMIT, new String(aux));
}

{Distinct} {
    String aux = yytext();
    return criaSimbolo(sym.DISTINCT, new String(aux));
}

{Database} {
    String aux = yytext();
    return criaSimbolo(sym.DATABASE, new String(aux));
}

{Truncate} {
    String aux = yytext();
    return criaSimbolo(sym.TRUNCATE, new String(aux));
}

{Drop} {
    String aux = yytext();
    return criaSimbolo(sym.DROP, new String(aux));
}

{Count} {
    String aux = yytext();
    return criaSimbolo(sym.COUNT, new String(aux));
}

{Max} {
    String aux = yytext();
    return criaSimbolo(sym.MAX, new String(aux));
}

{Min} {
    String aux = yytext();
    return criaSimbolo(sym.MIN, new String(aux));
}

{Avg} {
    String aux = yytext();
    return criaSimbolo(sym.AVG, new String(aux));
}

{Sum} {
    String aux = yytext();
    return criaSimbolo(sym.SUM, new String(aux));
}

{Null} {
    String aux = yytext();
    return criaSimbolo(sym.NULL, new String(aux));
}

{Default} {
    String aux = yytext();
    return criaSimbolo(sym.DEFAULT, new String(aux));
}

{In} {
    String aux = yytext();
    return criaSimbolo(sym.IN, new String(aux));
}
{Like} {
    String aux = yytext();
    return criaSimbolo(sym.LIKE, new String(aux));
}

{Or} {
    String aux = yytext();
    return criaSimbolo(sym.OR, new String(aux));
}

{And} {
    String aux = yytext();
    return criaSimbolo(sym.AND, new String(aux));
}

{Not} {
    String aux = yytext();
    return criaSimbolo(sym.NOT, new String(aux));
}

{As} {
    String aux = yytext();
    return criaSimbolo(sym.AS, new String(aux));
}

{Is} {
    String aux = yytext();
    return criaSimbolo(sym.IS, new String(aux));
}

{CurrentDate} {
    String aux = yytext();
    return criaSimbolo(sym.CURRENTDATE, new String(aux));
}

{Update} {
    String aux = yytext();
    return criaSimbolo(sym.UPDATE, new String(aux));
}

{Set} {
    String aux = yytext();
    return criaSimbolo(sym.SET, new String(aux));
}

{Delete} {
    String aux = yytext();
    return criaSimbolo(sym.DELETE, new String(aux));
}

{To} {
    String aux = yytext();
    return criaSimbolo(sym.TO, new String(aux));
}


{Ident} {
    String aux = yytext();
    return criaSimbolo(sym.IDENT, new String(aux));
}

{espaco} {/* despreza */ }

[^] { /* Caractere invalido */
    this.defineErro(yyline, yycolumn, "Erro Léxico - Simbolo desconhecido: " +yytext());
}
