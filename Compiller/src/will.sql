# s1 teste de select simples asterisco
select * from tbl_alunos;

-- s2 teste select simples com colunas
select id, nome from tbl_alunos;

-- s3 teste select com where simples
select id, nome from tbl_alunos where turma = '3E';

-- s4 teste select com where like
select id, nome from tbl_alunos where nome LIKE '%Ana%';

-- s5 select com order
SELECT * FROM Customers
ORDER BY Country DESC;

-- s6 select com order duplo
SELECT * FROM Customers
ORDER BY Country ASC, CustomerName DESC;

-- s7
SELECT * FROM Customers
WHERE NOT Country = 'Germany' AND NOT Country = 'USA';

--s8
SELECT * FROM Customers
WHERE (Country = 'Germany' AND City = 'Berlin' OR City = 'Stuttgart');

-- s9 avg
SELECT AVG(Price)
FROM Products;

--s10 sum
SELECT SUM(Quantity)
FROM OrderDetails;

--s11 min
SELECT MIN(Price)
FROM Products;

#s12 max
SELECT MAX(Price)
FROM Products;

--s13 between
SELECT * FROM Products
WHERE Price BETWEEN 10 AND 20;

--s14
SELECT COUNT(DISTINCT Country) FROM Customers;




-- a1 alter table simples
ALTER TABLE Customers
    ADD Email varchar(255);

-- a2 alter table simples drop
ALTER TABLE Customers
    DROP COLUMN Email;

-- a3
ALTER TABLE tbl_alunos
    ADD PRIMARY KEY (id),
    ADD KEY turma (turma);

--a4
ALTER TABLE tbl_alunos
    MODIFY id int(11) NOT NULL AUTO_INCREMENT;

--a5
ALTER TABLE tbl_alunos
    ADD CONSTRAINT tbl_alunos_ibfk_1 FOREIGN KEY (turma) REFERENCES tbl_turma (id);

--a6
ALTER TABLE Orders
    ADD FOREIGN KEY (PersonID) REFERENCES Persons(PersonID);

--a7
ALTER TABLE teste
    RENAME COLUMN old_name to new_name;

--a8
ALTER TABLE teste
    MODIFY COLUMN willian int(11);



/* c1 teste de create  com if not existes */
CREATE TABLE IF NOT EXISTS AtendimentoSquadXCliente32 (
                                                          id char NOT NULL,
                                                          squad double,
                                                          isUsed numeric NOT NULL DEFAULT 1,
                                                          PRIMARY KEY (id)
);

-- c2 create simples com alter
CREATE TABLE tbl_alunos (
                            id int(11) NOT NULL,
                            nome varchar(256) NOT NULL,
                            turma varchar(128) NOT NULL,
                            data_ingersso datetime NOT NULL DEFAULT CURRENT_DATE(),
                            ativo  tinyint(4) NOT NULL DEFAULT 1
);

-- c3
CREATE TABLE TestTable AS
SELECT customername, contactname
FROM customers;


-- c4
CREATE TABLE table_name AS
SELECT customername, contactname
FROM customers;

-- c5
CREATE TABLE `table_name` AS
SELECT customername, contactname
FROM customers;



/* i1 teste de insert correto com um valor */
INSERT INTO AtendimentoSquadXCliente32 (id, usuario, isUsed)  VALUES (3, 2, 1);

-- i2 teste de insert correto com valores
INSERT INTO AtendimentoSquadXCliente32 (id, usuario, isUsed)  VALUES
                                                                  (3, 2, 1),
                                                                  (3, 2, 1),
                                                                  (3, 2, 1);

/* i3 teste de insert incorreto com varios valores */
INSERT INTO AtendimentoSquadXCliente32 (id, usuario, isUsed, isUsed)  VALUES
                                                                          (3, 2, 1, 4),
                                                                          (3, 2, 1, 4),
                                                                          (3, 2, 1, 4, 5, 0, 9, 8),
                                                                          (3, 2, 'will', 5);


#create db
CREATE DATABASE dados_escola;

#truncate table
TRUNCATE TABLE tbl_alunos;

#drop table
DROP TABLE tbl_alunos;

# drop db
DROP DATABASE dados_escola;

-- u1 update simples
UPDATE Customers
SET ContactName = 'Alfred Schmidt', City = 'Frankfurt'
WHERE CustomerID = 1;

--u2
UPDATE Customers
SET PostalCode = 0000000
WHERE Country = 'Mexico';


# d1 delete com where
DELETE FROM Customers WHERE CustomerName='Alfreds Futterkiste';

#d2 delete simples
DELETE FROM Customers;
